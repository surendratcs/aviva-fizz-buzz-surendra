﻿//---------------------------------------------------------------------------------------------------------
// <copyright file="FizzBuzzModel.cs" company="Tcs">
//  Copyright (c) Tcs.  All rights reserved.
// </copyright>
//
////<summary>
// Model Class for FizzBuzz.
// </summary>
//---------------------------------------------------------------------------------------------------------
namespace FizzBuzz.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Model Class for FizzBuzz.
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// sets input number
        /// </summary>
        [Required(ErrorMessage = "{0} is required")]
        [Range(typeof(int), "1", "1000",
               ErrorMessage = "{0} can only be between {1} and {2}")]
        public int Number { get; set; }
    }
}