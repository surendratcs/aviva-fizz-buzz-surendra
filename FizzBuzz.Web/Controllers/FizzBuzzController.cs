﻿//---------------------------------------------------------------------------------------------------------
// <copyright file="FizzBuzzController.cs" company="Tcs">
//  Copyright (c) Tcs.  All rights reserved.
// </copyright>
//
////<summary>
// Controller class for FizzBuzz.
// </summary>
//---------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Web.Controllers
{
    using System;
    using System.Web.Mvc;
    using FizzBuzz.Web.Models;
    using Business;

    /// <summary>
    /// Controller class for FizzBuzz.
    /// </summary>
    public class FizzBuzzController : Controller
    {
        /// <summary>
        /// IFizzBuzzList object for dependency resolver
        /// </summary>
        private readonly IFizzBuzzList _fizzBuzzList;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzController"/> class to resolve dependency.
        /// </summary>
        /// <param name="fizzBuzzList">Dependency object</param>
        public FizzBuzzController(IFizzBuzzList fizzBuzzList)
        {
            this._fizzBuzzList = fizzBuzzList;
        }

        /// <summary>
        /// Get FizzBuzz View
        /// </summary>
        /// <returns>FizzBuzz View</returns>
        public ActionResult FizzBuzz()
        {
            return View("FizzBuzz");
        }

        /// <summary>
        /// Post method for FizzBuzz View.
        /// </summary>
        /// <param name="number"> input number</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult FizzBuzz(FizzBuzzModel number)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Data = this._fizzBuzzList.GetOutputList(number.Number, DateTime.Now.DayOfWeek);
                return View("FizzBuzzList", ViewBag.Data);
            }
            return View("FizzBuzz");
        }



    }
}
