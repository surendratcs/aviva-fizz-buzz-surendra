//---------------------------------------------------------------------------------------------------------
// <copyright file="DependencyReolver.cs" company="Tcs">
//  Copyright (c) Tcs.  All rights reserved.
// </copyright>
//
////<summary>
// Dependency injection resolver
// </summary>
//---------------------------------------------------------------------------------------------------------
namespace FizzBuzz.Web
{
    using System.Web.Mvc;
    using Business;
    using Microsoft.Practices.Unity;
    using Unity.Mvc4;

    /// <summary>
    /// Dependency injection resolver
    /// </summary>
    public static class DependencyReolver
    {
        /// <summary>
        /// Initialize for dependency container to resolve.
        /// </summary>
        /// <returns> Unity Container </returns>
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            return container;
        }

        /// <summary>
        /// Register for dependency
        /// </summary>
        /// <param name="container">container to register</param>
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IFizzBuzzList, FizzBuzzList>();
        }

        /// <summary>
        /// Build Unity Container by registering the dependency.
        /// </summary>
        /// <returns>Unity Container with registered types</returns>
        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            // e.g. container.RegisterType<ITestService, TestService>();    
            RegisterTypes(container);
            return container;
        }
    }
}