﻿//---------------------------------------------------------------------------------------------------------
// <copyright file="FizzBuzzListTest.cs" company="Tcs">
//  Copyright (c) Tcs.  All rights reserved.
// </copyright>
//
////<summary>
// FizzBuzzTest class to test FizzBuzz class functionalities.
// </summary>
//---------------------------------------------------------------------------------------------------------
namespace FizzBuzz.Business.Test
{
    using System;
    using System.Collections.Generic;
    using NUnit.Framework;

    /// <summary>
    /// FizzBuzzTest class to test FizzBuzz class functionalities.
    /// </summary>
    [TestFixture]
    public class FizzBuzzListTest
    {
        /// <summary>
        /// FizzBuzz for using methods of it.
        /// </summary>
        private FizzBuzzList fizzBuzz;

        /// <summary>
        /// Create instance for FizzBuzz.
        /// </summary>
        [SetUp]
        public void CreateFizzBuzzListInstance()
        {
            this.fizzBuzz = new FizzBuzzList();
        }

        /// <summary>
        /// Test method for total list count pass.
        /// </summary>
        [Test]
        public void Test_Total_List_Count_Pass()
        {
            List<string> list = this.fizzBuzz.GetOutputList(10);
            Assert.AreEqual(list.Count, 10);
        }

        /// <summary>
        /// Test method for total list count Fail
        /// </summary>
        [Test]
        public void Test_Total_List_Count_Fail()
        {
            List<string> list = this.fizzBuzz.GetOutputList(10);
            Assert.AreNotEqual(list.Count, 1);
        }

        /// <summary>
        /// Test for the Non Divisible number (which is not divisible by three,five) pass.
        /// </summary>
        [Test]
        public void Test_Non_Divisible_Number_Pass()
        {
            List<string> list = this.fizzBuzz.GetOutputList(1);
            Assert.AreEqual("1", list[list.Count - 1]);
        }

        /// <summary>
        /// Test for the Non Divisible number (which is not divisible by three ,five) fail.
        /// </summary>
        [Test]
        public void Test_Non_Divisible_Number_Fail()
        {
            List<string> list = this.fizzBuzz.GetOutputList(3);
            Assert.AreNotEqual(3, list[list.Count - 1]);
        }

        #region Divisible By Three
        /// <summary>
        /// Test method for divisible by three output for non wednesday pass.
        /// </summary>
        [Test]
        public void Test_Divisible_ByThree_OutPut_For_Non_Wednesday_Pass()
        {
            List<string> list = this.fizzBuzz.GetOutputList(3, DayOfWeek.Friday);
            Assert.AreEqual(DivisibleByThree.Fizz, list[list.Count - 1]);
        }

        /// <summary>
        /// Test method for divisible by three output for non wednesday Fail.
        /// </summary>
        [Test]
        public void Test_Divisible_ByThree_OutPut_For_Non_Wednesday_Fail()
        {
            List<string> list = this.fizzBuzz.GetOutputList(4, DayOfWeek.Friday);
            Assert.AreNotEqual(DivisibleByThree.Fizz, list[list.Count - 1]);
        }

        /// <summary>
        /// Test method for divisible by three output for wednesday pass.
        /// </summary>
        [Test]
        public void Test_Divisible_By_Three_OutPut_For_Wednesday_Pass()
        {
            List<string> list = this.fizzBuzz.GetOutputList(3);
            Assert.AreEqual(DivisibleByThree.Wizz, list[list.Count - 1]);
        }

        /// <summary>
        /// Test method for divisible by three output for wednesday Fail.
        /// </summary>
        [Test]
        public void Test_Divisible_By_Three_OutPut_For_Wednesday_Fail()
        {
            List<string> list = this.fizzBuzz.GetOutputList(4);
            Assert.AreNotEqual(DivisibleByThree.Wizz, list[list.Count - 1]);
        }

        #endregion

        #region Divisible By Five
        /// <summary>
        /// Test method for divisible by five output for non wednesday pass.
        /// </summary>
        [Test]
        public void Test_Divisible_By_Five_OutPut_For_Non_Wednesday_Pass()
        {
            List<string> list = this.fizzBuzz.GetOutputList(10, DayOfWeek.Monday);
            Assert.AreEqual(DivisibleByFive.Buzz, list[list.Count - 1]);
        }

        /// <summary>
        /// Test method for divisible by five output for non wednesday fail.
        /// </summary>
        [Test]
        public void Test_Divisible_By_Five_OutPut_For_Non_Wednesday_Fail()
        {
            List<string> list = this.fizzBuzz.GetOutputList(6, DayOfWeek.Monday);
            Assert.AreNotEqual(DivisibleByFive.Buzz, list[list.Count - 1]);
        }

        /// <summary>
        /// Test method for divisible by five output for wednesday pass.
        /// </summary>
        [Test]
        public void Test_Divisible_By_Five_OutPut_For_Wednesday_Pass()
        {
            List<string> list = this.fizzBuzz.GetOutputList(10);
            Assert.AreEqual(DivisibleByFive.Wuzz, list[list.Count - 1]);
        }

        /// <summary>
        /// Test method for divisible by five output for wednesday fail.
        /// </summary>
        [Test]
        public void Test_Divisible_By_Five_OutPut_For_Wednesday_Fail()
        {
            List<string> list = this.fizzBuzz.GetOutputList(6);
            Assert.AreNotEqual(DivisibleByFive.Wuzz, list[list.Count - 1]);
        }
        #endregion

        #region Divisible By Both Three and Five
        /// <summary>
        /// Test method for divisible by both three and five output for non wednesday pass.
        /// </summary>
        [Test]
        public void Test_Divisible_By_Three_And_Five_OutPut_For_Non_Wednesday_Pass()
        {
            List<string> list = this.fizzBuzz.GetOutputList(15, DayOfWeek.Saturday);
            Assert.AreEqual(DivisibleByThree.Fizz + " " + DivisibleByFive.Buzz, list[list.Count - 1]);
        }

        /// <summary>
        /// Test method for divisible by both three and five output for non wednesday fail.
        /// </summary>
        [Test]
        public void Test_Divisible_By_Three_And_Five_OutPut_For_Non_Wednesday_Fail()
        {
            List<string> list = this.fizzBuzz.GetOutputList(6, DayOfWeek.Saturday);
            Assert.AreNotEqual(DivisibleByThree.Fizz + " " + DivisibleByFive.Buzz, list[list.Count - 1]);
        }

        /// <summary>
        /// Test method for divisible by both three and five output for wednesday pass.
        /// </summary>
        [Test]
        public void Test_Divisible_By_Three_And_Five_OutPut_ForWednesday_Pass()
        {
            List<string> list = this.fizzBuzz.GetOutputList(15);
            Assert.AreEqual(DivisibleByThree.Wizz + " " + DivisibleByFive.Wuzz, list[list.Count - 1]);
        }

        /// <summary>
        /// Test method for divisible by both three and five output for wednesday fail.
        /// </summary>
        [Test]
        public void Test_Divisible_By_Three_And_Five_OutPut_For_Wednesday_Fail()
        {
            List<string> list = this.fizzBuzz.GetOutputList(6);
            Assert.AreNotEqual(DivisibleByThree.Wizz + " " + DivisibleByFive.Wuzz, list[list.Count - 1]);
        }
        #endregion
    }
}
