﻿//---------------------------------------------------------------------------------------------------------
// <copyright file="DivisibleByThreeTest.cs" company="Tcs">
//  Copyright (c) Tcs.  All rights reserved.
// </copyright>
//
////<summary>
// Test the Divisible by three functionalities.
// </summary>
//---------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Business.Test
{
    using System;
    using FizzBuzz.Business;
    using NUnit.Framework;

    /// <summary>
    /// Test the Divisible by three functionalities.
    /// </summary>
    [TestFixture]
    public class DivisibleByThreeTest
    {
        /// <summary>
        /// IDivisible for creating concrete class object.
        /// </summary>
        private IDivisible divisible;

        /// <summary>
        /// Create initial set up for the test methods.
        /// </summary>
        [SetUp]
        public void Create_Divisible_By_Three_Instance()
        {
            this.divisible = new DivisibleByThree();
        }

        /// <summary>
        /// Test number divisible by three pass
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Three_Pass()
        {
            Assert.AreEqual("1", this.divisible.DivisibleByNumber(1, DayOfWeek.Friday));
        }

        /// <summary>
        /// Test number divisible by three fail
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Three_Fail()
        {
            Assert.AreNotEqual("3", this.divisible.DivisibleByNumber(3, DayOfWeek.Friday));
        }
        
        /// <summary>
        /// Number divisible by three pass on non wednesday pass.
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Three_Non_Wednesday_Pass()
        {
            Assert.AreEqual(DivisibleByThree.Fizz, this.divisible.DivisibleByNumber(9, DayOfWeek.Friday));
        }

        /// <summary>
        /// Number divisible by three pass on non wednesday fail.
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Three_Non_Wednesday_Fail()
        {
            Assert.AreNotEqual(DivisibleByThree.Fizz, this.divisible.DivisibleByNumber(10, DayOfWeek.Friday));
        }

        /// <summary>
        /// Number divisible by three pass on wednesday pass.
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Three_Wednesday_Pass()
        {
            Assert.AreEqual(DivisibleByThree.Wizz, this.divisible.DivisibleByNumber(9, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Number divisible by three pass on  wednesday fail.
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Three_Wednesday_Fail()
        {
            Assert.AreNotEqual(DivisibleByThree.Wizz, this.divisible.DivisibleByNumber(10, DayOfWeek.Wednesday));
        }
    }
}
