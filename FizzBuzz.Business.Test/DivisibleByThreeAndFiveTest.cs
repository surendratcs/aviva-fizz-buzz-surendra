﻿//---------------------------------------------------------------------------------------------------------
// <copyright file="DivisibleByThreeAndFiveTest.cs" company="Tcs">
//  Copyright (c) Tcs.  All rights reserved.
// </copyright>
//
////<summary>
// Test the Divisible by three and five functionalities.
// </summary>
//---------------------------------------------------------------------------------------------------------
namespace FizzBuzz.Business.Test
{
    using System;
    using NUnit.Framework;

    /// <summary>
    /// Test the Divisible by three and five functionalities.
    /// </summary>
    [TestFixture]
    public class DivisibleByThreeAndFiveTest
    {
        /// <summary>
        /// IDivisible for creating concrete class object.
        /// </summary>
        private IDivisible divisible;

        /// <summary>
        /// Create initial set up for the test methods.
        /// </summary>
        [SetUp]
        public void Create_Divisible_By_Three_And__Five_Instance()
        {
            this.divisible = new DivisibleByThreeAndFive();
        }

        /// <summary>
        /// Test number divisible by three and five pass
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Three_And_Three_And__Five_Pass()
        {
            Assert.AreEqual("1", this.divisible.DivisibleByNumber(1, DayOfWeek.Friday));
        }

        /// <summary>
        /// Test number divisible by three and five fail
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Three_And__Five_Fail()
        {
            Assert.AreNotEqual("15", this.divisible.DivisibleByNumber(15, DayOfWeek.Friday));
        }

        /// <summary>
        /// Number divisible by three and five pass on non wednesday pass.
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Three_And__Five_Non_Wednesday_Pass()
        {
            Assert.AreEqual(DivisibleByThree.Fizz + " " + DivisibleByFive.Buzz, this.divisible.DivisibleByNumber(30, DayOfWeek.Friday));
        }

        /// <summary>
        /// Number divisible by three and five pass on non wednesday fail.
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Three_And__Five_Non_Wednesday_Fail()
        {
            Assert.AreNotEqual(DivisibleByThree.Fizz + " " + DivisibleByFive.Buzz, this.divisible.DivisibleByNumber(11, DayOfWeek.Friday));
        }

        /// <summary>
        /// Number divisible by three and five pass on wednesday pass.
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Three_And__Five_Wednesday_Pass()
        {
            Assert.AreEqual(DivisibleByThree.Wizz + " " + DivisibleByFive.Wuzz, this.divisible.DivisibleByNumber(15, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Number divisible by three and five pass on  wednesday fail.
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Three_And__Five_Wednesday_Fail()
        {
            Assert.AreNotEqual(DivisibleByThree.Wizz + " " + DivisibleByFive.Wuzz, this.divisible.DivisibleByNumber(11, DayOfWeek.Wednesday));
        }
    }
}
