﻿//---------------------------------------------------------------------------------------------------------
// <copyright file="DivisibleByFiveTest.cs" company="Tcs">
//  Copyright (c) Tcs.  All rights reserved.
// </copyright>
//
////<summary>
// Test the Divisible by five functionalities.
// </summary>
//---------------------------------------------------------------------------------------------------------
namespace FizzBuzz.Business.Test
{
    using System;
    using FizzBuzz.Business;
    using NUnit.Framework;

    /// <summary>
    /// Test the Divisible by five functionalities.
    /// </summary>
    [TestFixture]
    public class DivisibleByFiveTest
    {
        /// <summary>
        /// IDivisible for creating concrete class object.
        /// </summary>
        private IDivisible divisible;

        /// <summary>
        /// Create initial set up for the test methods.
        /// </summary>
        [SetUp]
        public void Create_Divisible_By_Five_Instance()
        {
            this.divisible = new DivisibleByFive();
        }

        /// <summary>
        /// Test number divisible by five pass
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Five_Pass()
        {
            Assert.AreEqual("1", this.divisible.DivisibleByNumber(1, DayOfWeek.Friday));
        }

        /// <summary>
        /// Test number divisible by five fail
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Five_Fail()
        {
            Assert.AreNotEqual("5", this.divisible.DivisibleByNumber(5, DayOfWeek.Friday));
        }

        /// <summary>
        /// Number divisible by five pass on non wednesday pass.
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Five_Non_Wednesday_Pass()
        {
            Assert.AreEqual(DivisibleByFive.Buzz, this.divisible.DivisibleByNumber(10, DayOfWeek.Friday));
        }

        /// <summary>
        /// Number divisible by five pass on non wednesday fail.
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Five_Non_Wednesday_Fail()
        {
            Assert.AreNotEqual(DivisibleByFive.Buzz, this.divisible.DivisibleByNumber(11, DayOfWeek.Friday));
        }

        /// <summary>
        /// Number divisible by five pass on wednesday pass.
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Five_Wednesday_Pass()
        {
            Assert.AreEqual(DivisibleByFive.Wuzz, this.divisible.DivisibleByNumber(10, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Number divisible by five pass on  wednesday fail.
        /// </summary>
        [Test]
        public void Test_Number_Divisible_By_Five_Wednesday_Fail()
        {
            Assert.AreNotEqual(DivisibleByFive.Wuzz, this.divisible.DivisibleByNumber(11, DayOfWeek.Wednesday));
        }
    }
}
