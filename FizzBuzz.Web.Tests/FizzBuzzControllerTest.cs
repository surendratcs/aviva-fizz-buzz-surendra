﻿//---------------------------------------------------------------------------------------------------------
// <copyright file="FizzBuzzControllerTest.cs" company="Tcs">
//  Copyright (c) Tcs.  All rights reserved.
// </copyright>
//
////<summary>
// Test class for FizzBuzzController.
// </summary>
//---------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Web.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using FizzBuzz.Web.Models;
    using FizzBuzz.Business;
    using FizzBuzz.Web.Controllers;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class for FizzBuzzController.
    /// </summary>
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        /// <summary>
        /// FizzBuzzController for using methods of it.
        /// </summary>
        private FizzBuzzController fizzbuzz;

        /// <summary>
        /// Mocking Fizz Buzz list object
        /// </summary>
        private Mock<IFizzBuzzList> mock;


        /// <summary>
        /// Create instance for FizzBuzzController.
        /// </summary>
        [SetUp]
        public void CreateFizzBuzzControllerInstance()
        {
            this.mock = new Mock<IFizzBuzzList>();
            this.mock.Setup(m => m.GetOutputList(16, DateTime.Now.DayOfWeek)).Returns(
                new List<string>() { "1", "2", "fizz", "4", "buzz", "fizz", "7", "8", "fizz", "buzz", "11", "fizz", "13", "14", "fizz buzz", "16" });
            this.fizzbuzz = new FizzBuzzController(this.mock.Object);
        }

        /// <summary>
        /// Fizz Buzz View Test
        /// </summary>
        [Test]
        public void Fizz_Buzz_View_Test()
        {
            var actionresult = this.fizzbuzz.FizzBuzz() as ViewResult;
            Assert.AreEqual(actionresult.ViewName, "FizzBuzz");
        }

        [Test]
        public void Fizz_Buzz_View_Test_Model_Pass()
        {
            var fizzbuzzMoldel = new FizzBuzzModel()
            {
                Number = 1200
            };
            var validationContext = new ValidationContext(fizzbuzzMoldel, null, null);
            var validationResults = new List<ValidationResult>();
            Validator.TryValidateObject(fizzbuzzMoldel, validationContext, validationResults, true);
            foreach (var validationResult in validationResults)
            {
                this.fizzbuzz.ModelState.AddModelError(validationResult.MemberNames.First(), validationResult.ErrorMessage);
            }
            var result = this.fizzbuzz.FizzBuzz(fizzbuzzMoldel) as ViewResult;
            Assert.AreEqual("FizzBuzz", result.ViewName);
        }

        [Test]
        public void Fizz_Buzz_View_Test_Model_Fail()
        {
            var fizzbuzzMoldel = new FizzBuzzModel()
            {
                Number = 120
            };
            var validationContext = new ValidationContext(fizzbuzzMoldel, null, null);
            var validationResults = new List<ValidationResult>();
            Validator.TryValidateObject(fizzbuzzMoldel, validationContext, validationResults, true);
            foreach (var validationResult in validationResults)
            {
                this.fizzbuzz.ModelState.AddModelError(validationResult.MemberNames.First(), validationResult.ErrorMessage);
            }
            var result = this.fizzbuzz.FizzBuzz(fizzbuzzMoldel) as ViewResult;
            Assert.AreEqual("FizzBuzzList", result.ViewName);
        }
    }
}
