﻿//---------------------------------------------------------------------------------------------------------
// <copyright file="DivisibleByThreeAndFive.cs" company="Tcs">
//  Copyright (c) Tcs.  All rights reserved.
// </copyright>
//
////<summary>
// DivisibleByFive class for both three and five divisions.
// </summary>
//---------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Business
{
    using System;
    using System.Globalization;

    /// <summary>
    /// DivisibleByThreeAndFive for both three and five divisions
    /// </summary>
   public class DivisibleByThreeAndFive : IDivisible
    {
        /// <summary>
        /// Check either the number is divisible by both three and five or not.
        /// </summary>
        /// <param name="number">given number</param>
        /// <param name="dayOfWeek">Current day</param>
        /// <returns>string value</returns>
        public string DivisibleByNumber(int number, DayOfWeek dayOfWeek)
        {
            return (number % 3 == 0 && number % 5 == 0) ? ((dayOfWeek != DayOfWeek.Wednesday)
                        ? DivisibleByThree.Fizz + " " + DivisibleByFive.Buzz : DivisibleByThree.Wizz + " " + DivisibleByFive.Wuzz)
                        : number.ToString(CultureInfo.CurrentCulture);
        }
    }
}
