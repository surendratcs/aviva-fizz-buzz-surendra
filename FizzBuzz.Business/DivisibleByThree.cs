﻿//---------------------------------------------------------------------------------------------------------
// <copyright file="DivisibleByThree.cs" company="Tcs">
//  Copyright (c) Tcs.  All rights reserved.
// </copyright>
//
////<summary>
//  Divisible by three functionalities.
// </summary>
//---------------------------------------------------------------------------------------------------------
namespace FizzBuzz.Business
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Divisible by three functionalities.
    /// </summary>
   public class DivisibleByThree : IDivisible
    {
        /// <summary>
        /// value for divisible by three
        /// </summary>
        public const string Fizz = "fizz";

        /// <summary>
        /// value for divisible by three for wednesday
        /// </summary>
        public const string Wizz = "wizz";

        /// <summary>
        /// Check the number is divisible by three.
        /// </summary>
        /// <param name="number">given number</param>
        /// /// <param name="dayOfWeek">Current day</param>
        /// <returns>string value</returns>
       public string DivisibleByNumber(int number, DayOfWeek dayOfWeek)
        {
            return number % 3 == 0 ?
                ((dayOfWeek != DayOfWeek.Wednesday) ? Fizz : Wizz)
                : number.ToString(CultureInfo.CurrentCulture);
        }
    }
}
