﻿//---------------------------------------------------------------------------------------------------------
// <copyright file="IFizzBuzzList.cs" company="Tcs">
//  Copyright (c) Tcs.  All rights reserved.
// </copyright>
//
////<summary>
// For FizzBuzz output list.
// </summary>
//---------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Business
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// FizzBuzz output list.
    /// </summary>
   public interface IFizzBuzzList
    {
        /// <summary>
        /// Abstract method to get the output list between one and given number
        /// </summary>
        /// <param name="number">Given input number</param>
        /// <param name="dayOfWeek">Current day </param>
        /// <returns>List of values between one and input number</returns>
        List<string> GetOutputList(int number, DayOfWeek dayOfWeek);
    }
}
