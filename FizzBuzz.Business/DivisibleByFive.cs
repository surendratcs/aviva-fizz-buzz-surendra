﻿//---------------------------------------------------------------------------------------------------------
// <copyright file="DivisibleByFive.cs" company="Tcs">
//  Copyright (c) Tcs.  All rights reserved.
// </copyright>
//
////<summary>
//  Divisible by fivr functionalities.
// </summary>
//---------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Business
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Divisible by five functionalities.
    /// </summary>
   public class DivisibleByFive : IDivisible
    {
        /// <summary>
        /// value for divisible by Five
        /// </summary>
        public const string Buzz = "buzz";

        /// <summary>
        /// value for divisible by five for wednesday
        /// </summary>
        public const string Wuzz = "wuzz";

        /// <summary>
        /// Check the number is divisible by five.
        /// </summary>
        /// <param name="number">given number</param>
        /// /// <param name="dayOfWeek">Current day</param>
        /// <returns>string value</returns>
        public string DivisibleByNumber(int number, DayOfWeek dayOfWeek)
        {
            return number % 5 == 0 ?
                 ((dayOfWeek != DayOfWeek.Wednesday) ? Buzz : Wuzz)
                 : number.ToString(CultureInfo.CurrentCulture);
        }
    }
}
