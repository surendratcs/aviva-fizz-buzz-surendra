﻿//---------------------------------------------------------------------------------------------------------
// <copyright file="FizzBuzzList.cs" company="Tcs">
//  Copyright (c) Tcs.  All rights reserved.
// </copyright>
//
////<summary>
// For FizzBuzz output list.
// </summary>
//---------------------------------------------------------------------------------------------------------

namespace FizzBuzz.Business
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// FizzBuzz output list.
    /// </summary>
    public class FizzBuzzList : IFizzBuzzList
    {
        /// <summary>
        /// IDivisible list
        /// </summary>
        private readonly List<IDivisible> divisibles;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzList"/> class
        /// </summary>
        public FizzBuzzList()
        {
            this.divisibles = new List<IDivisible>
                {
                    new DivisibleByThreeAndFive(),
                    new DivisibleByThree(),
                    new DivisibleByFive()
                };
        }

        /// <summary>
        /// Get Output list between one and given number
        /// </summary>
        /// <param name="number">Given input number</param>
        /// <param name="dayOfWeek">Current day default to Wednesday</param>
        /// <returns>List of values between one and input number</returns>
        public List<string> GetOutputList(int number, DayOfWeek dayOfWeek = DayOfWeek.Wednesday)
        {
            var list = new List<string>();
            string output = string.Empty;
            for (int i = 1; i <= number; i++)
            {
                foreach (var divisible in this.divisibles)
                {
                    output = divisible.DivisibleByNumber(i, dayOfWeek);
                    if (output != i.ToString(CultureInfo.CurrentCulture))
                    {
                        break;
                    }
                }

                list.Add(output);
            }

            return list;
        }
    }
}
