﻿//---------------------------------------------------------------------------------------------------------
// <copyright file="IDivisible.cs" company="Tcs">
//  Copyright (c) Tcs.  All rights reserved.
// </copyright>
//
////<summary>
// IDivisible interface for division.
// </summary>
//---------------------------------------------------------------------------------------------------------
namespace FizzBuzz.Business
{
    using System;

    /// <summary>
    ///  Parent division interface.
    /// </summary>
   public interface IDivisible
    {
        /// <summary>
        /// Parent division interface
        /// </summary>
        /// <param name="number">number to be evaluated</param>
        /// <param name="dayOfWeek">Current day</param>
        /// <returns>string based on the divisible condition</returns>
        string DivisibleByNumber(int number, DayOfWeek dayOfWeek); 
    }
}
